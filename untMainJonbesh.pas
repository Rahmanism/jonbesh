unit untMainJonbesh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, MPlayer, Buttons, ComCtrls, VolCtrl, jpeg,
  ActnList, ShellAPI, Menus, OleCtrls, ShockwaveFlashObjects_TLB, ImgList;

const
  MaxNo = 9;
  WM_ICONTRAY = WM_USER + 1;

type
  TfrmMainJonbesh = class(TForm)
    Timer1: TTimer;
    lbl1: TLabel;
    lbl4: TLabel;
    imgMain: TImage;
    img1: TImage;
    img2: TImage;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    lbl7: TLabel;
    lbl8: TLabel;
    img3: TImage;
    img4: TImage;
    img5: TImage;
    img6: TImage;
    img7: TImage;
    img8: TImage;
    imgAbout: TImage;
    lblAbout: TLabel;
    imgMin: TImage;
    lblMin: TLabel;
    imgExit: TImage;
    lblExit: TLabel;
    ActionList1: TActionList;
    actPlayPause: TAction;
    actStop: TAction;
    actRewind: TAction;
    actForward: TAction;
    actPrevious: TAction;
    actNext: TAction;
    actVolHigh: TAction;
    actVolLow: TAction;
    actMute: TAction;
    MediaPlayer1: TMediaPlayer;
    lbl9: TLabel;
    img9: TImage;
    tmrBlend: TTimer;
    mplBack: TMediaPlayer;
    imgMusic: TImage;
    lblMusic: TLabel;
    pnlScreen: TPanel;
    pnlMediaPlayer: TPanel;
    imgStop: TImage;
    imgRewind: TImage;
    imgRepeat: TImage;
    imgPre: TImage;
    imgPlay: TImage;
    imgPauseO: TImage;
    imgPause: TImage;
    imgNext: TImage;
    imgLoop: TImage;
    imgForward: TImage;
    lblTime: TLabel;
    lblStop: TLabel;
    lblRewind: TLabel;
    lblRepeat: TLabel;
    lblPrevious: TLabel;
    lblPlayPause: TLabel;
    lblNext: TLabel;
    lblLoop: TLabel;
    lblFullTime: TLabel;
    lblForward: TLabel;
    imgPlayerWithScreen: TImage;
    imgPlayer: TImage;
    imgTrackMove: TImage;
    lblTrackUser: TLabel;
    imgPreD: TImage;
    imgRwD: TImage;
    imgStopD: TImage;
    imgFwD: TImage;
    imgNextD: TImage;
    imgRepD: TImage;
    imgLoopD: TImage;
    imgPauseD: TImage;
    imgPlayD: TImage;
    imgSound: TImage;
    lblChangeSound: TLabel;
    lblSysTray: TLabel;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    imgSysTray: TImage;
    actFullScreen: TAction;
    imgMute: TImage;
    imgMuteD: TImage;
    lblMute: TLabel;
    ImageList1: TImageList;
    mVolControl1: TmVolControl;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure MediaPlayer1Notify(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lblLoopClick(Sender: TObject);
    procedure lblRepeatClick(Sender: TObject);
    procedure lbl1Click(Sender: TObject);
    procedure lbl4Click(Sender: TObject);
    procedure lblStopClick(Sender: TObject);
    procedure lblPlayPauseClick(Sender: TObject);
    procedure lblPlayPauseMouseEnter(Sender: TObject);
    procedure lblPlayPauseMouseLeave(Sender: TObject);
    procedure lblStopMouseEnter(Sender: TObject);
    procedure lblStopMouseLeave(Sender: TObject);
    procedure lblRepeatMouseEnter(Sender: TObject);
    procedure lblRepeatMouseLeave(Sender: TObject);
    procedure lblLoopMouseEnter(Sender: TObject);
    procedure lblLoopMouseLeave(Sender: TObject);
    procedure lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbl1MouseEnter(Sender: TObject);
    procedure lbl1MouseLeave(Sender: TObject);
    procedure PlayFile(FileNo: Byte);
    procedure Imaging(No: Byte);
    procedure lbl4MouseEnter(Sender: TObject);
    procedure lblForwardClick(Sender: TObject);
    procedure lblRewindClick(Sender: TObject);
    procedure lblNextClick(Sender: TObject);
    procedure lblPreviousClick(Sender: TObject);
    procedure actVolHighExecute(Sender: TObject);
    procedure actVolLowExecute(Sender: TObject);
    procedure lblPreviousMouseEnter(Sender: TObject);
    procedure lblPreviousMouseLeave(Sender: TObject);
    procedure lblRewindMouseEnter(Sender: TObject);
    procedure lblRewindMouseLeave(Sender: TObject);
    procedure lblForwardMouseEnter(Sender: TObject);
    procedure lblForwardMouseLeave(Sender: TObject);
    procedure lblNextMouseEnter(Sender: TObject);
    procedure lblNextMouseLeave(Sender: TObject);
    procedure lbl2MouseEnter(Sender: TObject);
    procedure lbl3MouseEnter(Sender: TObject);
    procedure lbl3Click(Sender: TObject);
    procedure lbl5MouseEnter(Sender: TObject);
    procedure lbl6MouseEnter(Sender: TObject);
    procedure lbl7MouseEnter(Sender: TObject);
    procedure lblAboutMouseEnter(Sender: TObject);
    procedure lblAboutMouseLeave(Sender: TObject);
    procedure lblMinMouseEnter(Sender: TObject);
    procedure lblMinMouseLeave(Sender: TObject);
    procedure lblMinClick(Sender: TObject);
    procedure lblExitMouseEnter(Sender: TObject);
    procedure lblExitMouseLeave(Sender: TObject);
    procedure lblExitClick(Sender: TObject);
    procedure lbl2Click(Sender: TObject);
    procedure lbl5Click(Sender: TObject);
    procedure lbl6Click(Sender: TObject);
    procedure lbl7Click(Sender: TObject);
    procedure lbl8Click(Sender: TObject);
    procedure lblAboutClick(Sender: TObject);
    procedure lblChangeSoundMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lbl9Click(Sender: TObject);
    procedure lbl9MouseEnter(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tmrBlendTimer(Sender: TObject);
    procedure mplBackNotify(Sender: TObject);
    procedure lblMusicMouseEnter(Sender: TObject);
    procedure lblMusicMouseLeave(Sender: TObject);
    procedure lblMusicClick(Sender: TObject);
    procedure lbl8MouseEnter(Sender: TObject);
    procedure lblPreviousMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPreviousMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRewindMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblStopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblForwardMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblNextMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblRepeatMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblLoopMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblPlayPauseMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblSysTrayClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure lblSysTrayMouseEnter(Sender: TObject);
    procedure lblSysTrayMouseLeave(Sender: TObject);
    procedure actFullScreenExecute(Sender: TObject);
    procedure pnlScreenDblClick(Sender: TObject);
    procedure actMuteExecute(Sender: TObject);
    procedure lblMuteClick(Sender: TObject);
    procedure lblMuteMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure lblMuteMouseEnter(Sender: TObject);
    procedure lblMuteMouseLeave(Sender: TObject);
  private
    TrayIconData: TNotifyIconData;
    procedure InitialTrayIcon;
    procedure TrayMessage(var Msg: TMessage); message WM_ICONTRAY;
    function MiliSecondToTime(Sec: Longint): AnsiString;
    procedure UpdateCurrTime;
    function UpdateFileName(n: Byte): String;
    procedure SoundHeightShow;
    procedure ChangeSound(v: Smallint);
    procedure Imaging1;//(No: Integer);
//    function GetWinDir: TFileName;
    { Private declarations }
  public
    { Public declarations }
  end;

procedure SetMute;
procedure ResetMute;
function MyDir(S: String): String;

var
  frmMainJonbesh: TfrmMainJonbesh;
  NowPlay: Byte;  // 0=none
  Vol: Byte;
  Mute: Boolean;
  Rep, Loop: Boolean;
  FilmPath, DataPath: String;
  mplBackPlay: Boolean;

implementation

uses Math, untAbout, untSplash, untBackground, untFullScreen, Types,
  untSplashLogo;

{$R *.dfm}

function MyDir(S: String): String;
begin
  S := GetCurrentDir + '\' + S;
  while Pos('\\', S) > 0 do
    Delete(S, Pos('\\', S), 1);
  Result := S;
end;

procedure TfrmMainJonbesh.InitialTrayIcon;
begin
  PopUpMenu1.OwnerDraw:=True;

  with TrayIconData do
  begin
    cbSize := SizeOf(TrayIconData);
    Wnd := Handle;
    uID := 0;
    uFlags := NIF_MESSAGE + NIF_ICON + NIF_TIP;
    uCallbackMessage := WM_ICONTRAY;
    hIcon := Application.Icon.Handle;
    StrPCopy(szTip, Application.Title);
  end;

  Shell_NotifyIcon(NIM_ADD, @TrayIconData);
end;

procedure TfrmMainJonbesh.TrayMessage(var Msg: TMessage);
var
  p : TPoint;
begin
{  case Msg.lParam of
    WM_LBUTTONDOWN:
    begin
      ShowMessage('This icon responds to RIGHT BUTTON click!');
    end;
    WM_RBUTTONDOWN:
    begin
       SetForegroundWindow(Handle);
       GetCursorPos(p);
       PopUpMenu1.Popup(p.x, p.y);
       PostMessage(Handle, WM_NULL, 0, 0);
    end;
  end;}
  if (Msg.LParam = WM_LBUTTONDOWN) or (Msg.LParam = WM_RBUTTONDOWN) then
  begin
    SetForegroundWindow(Handle);
    GetCursorPos(p);
    PopUpMenu1.Popup(p.x, p.y);
    PostMessage(Handle, WM_NULL, 0, 0);
  end;
end;

procedure TfrmMainJonbesh.FormCreate(Sender: TObject);
begin
  frmSplashLogo := TfrmSplashLogo.Create(Self);
  frmSplashLogo.ShowModal;

  frmBackground := TfrmBackground.Create(Self);
  frmBackground.Show;
  frmBackground.Enabled := False;

  swfMovie := MyDir('Data\Sys02.dll');
  frmSplash := TfrmSplash.Create(Self);
  frmSplash.ShowModal;

  FilmPath := MyDir('Film\');
  DataPath := MyDir('Data\');
  Rep := False;
  Loop := False;
  NowPlay := 0; // No file is playing.
  Vol := mVolControl1.Volume;
  Mute := False;
  if Mute then
    imgMute.Visible := True
  else
    imgMute.Visible := False;
  SoundHeightShow;
  try
    mplBack.FileName := MyDir('Data\b.mp3');
    mplBack.Open;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
  except;end;
end;

procedure SetMute;
begin
  Mute := True;
end;

procedure ResetMute;
begin
  Mute := False;
end;

(*function TForm1.GetWinDir: TFileName;
var
  SysFolder: String;
begin
  SetLength(SysFolder, 255);
  SetLength(SysFolder, GetWindowsDirectory(
    PChar(SysFolder), Length(SysFolder)));
  Result := SysFolder;
end;*)

procedure TfrmMainJonbesh.UpdateCurrTime;
var
  cp, l, t: Longint; // Current Position, Length
begin
  try
    cp := MediaPlayer1.Position;// div 1000;
    l := MediaPlayer1.Length;// div 1000;
    t := cp * lblTrackUser.Width div l;
    if not (t >= 344) then  // 345 is the length of lblTrackbar
      t := (t div 5) * 5;
    imgTrackMove.Width := t;
//    imgTrackMove.Width := lblTrackUser.Width - t;
//    imgTrackMove.Left := lblTrackUser.Left + t;
    lblTime.Caption := MiliSecondToTime(cp);
  except
    ;
  end;
end;

procedure TfrmMainJonbesh.Timer1Timer(Sender: TObject);
begin
  try
    UpdateCurrTime;
    if MediaPlayer1.Position
      >= MediaPlayer1.Length then
    begin
      if Rep then
      begin
        MediaPlayer1.Rewind;
        MediaPlayer1.Play;
        MediaPlayer1.Notify := True;
      end
      else
      begin
        if Loop then
        begin
     //     if NowPlay in [1..MaxFileNo] then
     //     begin
            if NowPlay + 1 > MaxNo then
            begin
              PlayFile(1)
            end
            else
              PlayFile(NowPlay+1);
      //    end;
        end
        else
          lblStopClick(Sender);
      end;
    end;
  except
    ;
  end;
end;

procedure TfrmMainJonbesh.MediaPlayer1Notify(Sender: TObject);
begin
      try
        if (mplBack.Mode = mpPlaying) and (MediaPlayer1.Mode = mpPlaying) then
        begin
          mplBack.Stop;
          mplBackPlay := False;
        end;
        mplBack.Notify := True;
      except;end;
  with Sender as TMediaPlayer do
  begin
    Notify := True;
    if (Mode = mpPaused) or (Mode = mpStopped) then
    begin
      imgPause.Visible := False;
      imgPauseO.Visible := False;
    end
    else if Mode = mpPlaying then
    begin
      imgPlay.Visible := False;
      imgPause.Visible := True;
    end;
  end;
end;

procedure TfrmMainJonbesh.FormShow(Sender: TObject);
begin
  tmrBlend.Enabled := True;
  MediaPlayer1.Notify := True;
end;

function TfrmMainJonbesh.UpdateFileName(n: Byte): String;
var
  fn: String;
begin
  fn := MyDir('Film\' + IntToStr(n) + '.wmv');
  if fn <> MediaPlayer1.FileName then
  begin
   // imgPlayerTitle.Picture.LoadFromFile(DataPath + IntToStr(n) + '.jpg');
    try
      MediaPlayer1.Close;
    except
      ;
    end;
    imgTrackMove.Width := 0;
    try
      MediaPlayer1.FileName := fn;
      MediaPlayer1.Open;
      lblPlayPause.Enabled := True;
      lblFullTime.Caption := MiliSecondToTime(MediaPlayer1.Length);
      NowPlay := n;
      Imaging1;
    except;end;
//    Imaging(NowPlay);
  end;
end;

procedure TfrmMainJonbesh.PlayFile(FileNo: Byte);
var
  t: TRect;
begin
  imgPlayerWithScreen.Visible := True;
  lblTrackUser.Visible := True;
  pnlMediaPlayer.Visible := True;
  try
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
    mplBack.Notify := True;
  except;end;
  UpdateFileName(FileNo);
  //pnlScreen.Visible := True;
  try
    MediaPlayer1.Play;
    MediaPlayer1Notify(MediaPlayer1);
    Timer1.Enabled := True;
  except; end;
  with t do
  begin
    Left := 0;
    Top := 0;
    Right := MediaPlayer1.Display.Width;
    Bottom := MediaPlayer1.Display.Height;
  end;
  MediaPlayer1.DisplayRect := t;
end;

function TfrmMainJonbesh.MiliSecondToTime(Sec: Longint): AnsiString;
var
  h, m, s: Byte;
  TimeStr: AnsiString;
begin
  Sec := Sec div 1000;
  m := Sec div 60;
  s := Sec - (m * 60);
  h := 0;
  if m >= 60 then
  begin
    h := m div 60;
    m := m - (h * 60);
  end;
  TimeStr := '';
  if h <> 0 then
    if m < 10 then
      TimeStr := IntToStr(h) + ':0'
    else
      TimeStr := IntToStr(h) + ':';
  TimeStr := TimeStr + IntToStr(m) + ':';
  if s < 10 then
    TimeStr := TimeStr + '0';
  TimeStr := TimeStr + IntToStr(s);
  Result := TimeStr;
end;

procedure TfrmMainJonbesh.Imaging1;//(No: Integer);
begin
  img1.Visible := False;
  img2.Visible := False;
  img3.Visible := False;
  img4.Visible := False;
  img5.Visible := False;
  img6.Visible := False;
  img7.Visible := False;
  img8.Visible := False;
  img9.Visible := False;
  case NowPlay of
    1: img1.Visible := True;
    2: img2.Visible := True;
    3: img3.Visible := True;
    4: img4.Visible := True;
    5: img5.Visible := True;
    6: img6.Visible := True;
    7: img7.Visible := True;
    8: img8.Visible := True;
    9: img9.Visible := True;
  end;
end;

procedure TfrmMainJonbesh.Imaging(No: Byte);
begin
  if No <> 0 then
    PlayFile(No);
  Imaging1;//(No);
end;

procedure TfrmMainJonbesh.lblLoopClick(Sender: TObject);
begin
  Loop := not Loop;
  if Loop then
  begin
    Rep := False;
    imgRepeat.Visible := False;
  end;
end;

procedure TfrmMainJonbesh.lblRepeatClick(Sender: TObject);
begin
  Rep := not Rep;
  if Rep then
  begin
    Loop := False;
    imgLoop.Visible := False;
  end;
end;

procedure TfrmMainJonbesh.SoundHeightShow;
begin
  imgSound.Width := (Vol * lblChangeSound.Width) div 255;
end;

procedure TfrmMainJonbesh.lbl1Click(Sender: TObject);
begin
  Imaging(1);
end;

procedure TfrmMainJonbesh.lbl4Click(Sender: TObject);
begin
  Imaging(4);
end;

procedure TfrmMainJonbesh.lblStopClick(Sender: TObject);
begin
  MediaPlayer1.Notify := True;
  try
//    imgTrackMove.Width := lblTrackUser.Width;
    imgTrackMove.Left := lblTrackUser.Left;
    Timer1.Enabled := False;
    MediaPlayer1.Rewind;
    MediaPlayer1.Stop;
    imgTrackMove.Width := 0;
  except
    ;
  end;
end;

procedure TfrmMainJonbesh.lblPlayPauseClick(Sender: TObject);
begin
  try
    MediaPlayer1.Notify := True;
    if NowPlay <> 0 then
    begin
      if imgPause.Visible then
        MediaPlayer1.Stop
      else
      begin
        Timer1.Enabled := True;
        MediaPlayer1.Play;
        MediaPlayer1Notify(MediaPlayer1);
      end;
    end;
  except;end;
end;

procedure TfrmMainJonbesh.lblPlayPauseMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseO.Visible := True
    else if not imgPause.Visible then
      imgPlay.Visible := True;
end;

procedure TfrmMainJonbesh.lblPlayPauseMouseLeave(Sender: TObject);
begin
  if imgPauseO.Visible then
    imgPauseO.Visible := False;
  if imgPlay.Visible then
    imgPlay.Visible := False;
end;

procedure TfrmMainJonbesh.lblStopMouseEnter(Sender: TObject);
begin
  if (NowPlay <> 0) then
    imgStop.Visible := True;
end;

procedure TfrmMainJonbesh.lblStopMouseLeave(Sender: TObject);
begin
  imgStop.Visible := False;
end;

procedure TfrmMainJonbesh.lblRepeatMouseEnter(Sender: TObject);
begin
  imgRepeat.Visible := True;
end;

procedure TfrmMainJonbesh.lblRepeatMouseLeave(Sender: TObject);
begin
  if not Rep then
    imgRepeat.Visible := False;
end;

procedure TfrmMainJonbesh.lblLoopMouseEnter(Sender: TObject);
begin
  imgLoop.Visible := True;
end;

procedure TfrmMainJonbesh.lblLoopMouseLeave(Sender: TObject);
begin
  if not Loop then
    imgLoop.Visible := False;
end;

procedure TfrmMainJonbesh.lblTrackUserMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := MediaPlayer1.Length;// div 1000;
//    t := (lblTrackUser.Width - X) * l
  //    div lblTrackUser.Width;
    t := X * l div lblTrackUser.Width;
    if not Timer1.Enabled then
    begin
//      imgTrackMove.Width := lblTrackUser.Width - X;
//      imgTrackMove.Left := lblTrackUser.Left + X;
      imgTrackMove.Width := X;
    end;
    MediaPlayer1.Position := t;
    MediaPlayer1.Notify := True;
    if imgPause.Visible then
    begin
      MediaPlayer1.Play;
      MediaPlayer1.Notify := True;
    end;
  except
    ;
  end;
end;

procedure TfrmMainJonbesh.lbl1MouseEnter(Sender: TObject);
begin
  img1.Visible := True;
end;

procedure TfrmMainJonbesh.lbl1MouseLeave(Sender: TObject);
begin
  Imaging(0);
end;

procedure TfrmMainJonbesh.lbl4MouseEnter(Sender: TObject);
begin
  img4.Visible := True;
end;

procedure TfrmMainJonbesh.lblForwardClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position + 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMainJonbesh.lblRewindClick(Sender: TObject);
var
  Playing: Boolean;
begin
  if NowPlay <> 0 then
  begin
    if MediaPlayer1.Mode = mpPlaying then
      Playing := True
    else
      Playing := False;
    MediaPlayer1.Position := MediaPlayer1.Position - 10000;
    UpdateCurrTime;
    if Playing then
      MediaPlayer1.Play;
    MediaPlayer1.Notify := True;
  end;
end;

procedure TfrmMainJonbesh.lblNextClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay + 1;
      if NowPlay > MaxNo then
      begin
        NowPlay := 1;
      end;
      if (MediaPlayer1.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    MediaPlayer1.Notify := True;
  except;end;
end;

procedure TfrmMainJonbesh.lblPreviousClick(Sender: TObject);
begin
  try
    if NowPlay <> 0 then
    begin
      NowPlay := NowPlay - 1;
      if NowPlay < 1 then
      begin
        NowPlay := MaxNo;
      end;
      if (MediaPlayer1.Mode in
        [mpPlaying, mpPaused]) then
        PlayFile(NowPlay)
      else
        UpdateFileName(NowPlay);
    end;
    MediaPlayer1.Notify := True;
  except;end;
end;

procedure TfrmMainJonbesh.actVolHighExecute(Sender: TObject);
begin
  ChangeSound(Vol + 26);
end;

procedure TfrmMainJonbesh.actVolLowExecute(Sender: TObject);
begin
  ChangeSound(Vol - 26);
end;

procedure TfrmMainJonbesh.lblPreviousMouseEnter(Sender: TObject);
begin
  imgPre.Visible := True;
end;

procedure TfrmMainJonbesh.lblPreviousMouseLeave(Sender: TObject);
begin
  imgPre.Visible := False;
end;

procedure TfrmMainJonbesh.lblRewindMouseEnter(Sender: TObject);
begin
  imgRewind.Visible := True;
end;

procedure TfrmMainJonbesh.lblRewindMouseLeave(Sender: TObject);
begin
  imgRewind.Visible := False;
end;

procedure TfrmMainJonbesh.lblForwardMouseEnter(Sender: TObject);
begin
  imgForward.Visible := True;
end;

procedure TfrmMainJonbesh.lblForwardMouseLeave(Sender: TObject);
begin
  imgForward.Visible := False;
end;

procedure TfrmMainJonbesh.lblNextMouseEnter(Sender: TObject);
begin
  imgNext.Visible := True;
end;

procedure TfrmMainJonbesh.lblNextMouseLeave(Sender: TObject);
begin
  imgNext.Visible := False;
end;

procedure TfrmMainJonbesh.lbl2MouseEnter(Sender: TObject);
begin
  img2.Visible := True;
end;

procedure TfrmMainJonbesh.lbl3MouseEnter(Sender: TObject);
begin
  img3.Visible := True;
end;

procedure TfrmMainJonbesh.lbl3Click(Sender: TObject);
begin
  Imaging(3);
end;

procedure TfrmMainJonbesh.lbl5MouseEnter(Sender: TObject);
begin
  img5.Visible := True;
end;

procedure TfrmMainJonbesh.lbl6MouseEnter(Sender: TObject);
begin
  img6.Visible := True;
end;

procedure TfrmMainJonbesh.lbl7MouseEnter(Sender: TObject);
begin
  img7.Visible := True;
end;

procedure TfrmMainJonbesh.lblAboutMouseEnter(Sender: TObject);
begin
  imgAbout.Visible := True;
end;

procedure TfrmMainJonbesh.lblAboutMouseLeave(Sender: TObject);
begin
  imgAbout.Visible := False;
end;

procedure TfrmMainJonbesh.lblMinMouseEnter(Sender: TObject);
begin
  imgMin.Visible := True;
end;

procedure TfrmMainJonbesh.lblMinMouseLeave(Sender: TObject);
begin
  imgMin.Visible := False;
end;

procedure TfrmMainJonbesh.lblMinClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmMainJonbesh.lblExitMouseEnter(Sender: TObject);
begin
  imgExit.Visible := True;
end;

procedure TfrmMainJonbesh.lblExitMouseLeave(Sender: TObject);
begin
  imgExit.Visible := False;
end;

procedure TfrmMainJonbesh.lblExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainJonbesh.lbl2Click(Sender: TObject);
begin
  Imaging(2);
end;

procedure TfrmMainJonbesh.lbl5Click(Sender: TObject);
begin
  Imaging(5);
end;

procedure TfrmMainJonbesh.lbl6Click(Sender: TObject);
begin
  Imaging(6);
end;

procedure TfrmMainJonbesh.lbl7Click(Sender: TObject);
begin
  Imaging(7);
end;

procedure TfrmMainJonbesh.lbl8Click(Sender: TObject);
begin
  Imaging(8);
end;

procedure TfrmMainJonbesh.lblAboutClick(Sender: TObject);
begin
  frmAbout := TfrmAbout.Create(Self);
  frmAbout.ShowModal;
end;

procedure TfrmMainJonbesh.ChangeSound(v: Smallint);
begin
  if v < 0 then
    v := 0
  else if v > 255 then
    v := 255;
  Vol := v;
  SoundHeightShow;
  if not Mute then
    mVolControl1.Volume := Vol;
end;

procedure TfrmMainJonbesh.lblChangeSoundMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  l, t: Longint; // Length
begin
  try
    l := 255;
    t := X * l div lblChangeSound.Width;
    ChangeSound(t);
  except
    ;
  end;
end;

procedure TfrmMainJonbesh.lbl9Click(Sender: TObject);
begin
  Imaging(9);
end;

procedure TfrmMainJonbesh.lbl9MouseEnter(Sender: TObject);
begin
  img9.Visible := True;
end;

procedure TfrmMainJonbesh.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  try
    mplBack.Close;
    MediaPlayer1.Close;
    Timer1.Enabled := False;
  except;end;
  if Tag = 0 then
  begin
    CanClose := False;
    frmMainJonbesh.AlphaBlend := True;
    Tag := 1;
    tmrBlend.Enabled := True;
  end;
end;

procedure TfrmMainJonbesh.tmrBlendTimer(Sender: TObject);
var
  ab: Integer;
begin
  if Tag = 0 then
  begin
    ab := frmMainJonbesh.AlphaBlendValue + 50;
    if ab > 255 then
    begin
      ab := 255;
      frmMainJonbesh.AlphaBlendValue := ab;
      frmMainJonbesh.AlphaBlend := False;
      tmrBlend.Enabled := False;
    end
    else
      frmMainJonbesh.AlphaBlendValue := ab;
  end
  else if Tag = 1 then
  begin
    ab := frmMainJonbesh.AlphaBlendValue - 50;
    if ab < 0 then
    begin
      ab := 0;
      frmMainJonbesh.AlphaBlendValue := ab;
      tmrBlend.Enabled := False;
      swfMovie := MyDir('Data\Sys03.dll');
      frmSplash := TfrmSplash.Create(Self);
//      frmSplash.Timer1.Interval := 1000;//47000;
      frmSplash.ShowModal;
      Close;
    end
    else
      frmMainJonbesh.AlphaBlendValue := ab;
  end
end;

procedure TfrmMainJonbesh.mplBackNotify(Sender: TObject);
begin
  try
    if (mplBack.Mode = mpStopped) and (mplBackPlay = True) then
    begin
      mplBack.Play;
      mplBackPlay := True;
    end
    else if mplBack.Position >= mplBack.Length then
    begin
      mplBack.Rewind;
      mplBack.Play;
    end;
  except;end;
  mplBack.Notify := True;
end;

procedure TfrmMainJonbesh.lblMusicMouseEnter(Sender: TObject);
begin
  imgMusic.Visible := True;
end;

procedure TfrmMainJonbesh.lblMusicMouseLeave(Sender: TObject);
begin
  imgMusic.Visible := False;
end;

procedure TfrmMainJonbesh.lblMusicClick(Sender: TObject);
begin
//  Imaging(0);
  try
    MediaPlayer1.Notify := False;
    if MediaPlayer1.Mode = mpPlaying then
      MediaPlayer1.Stop;
    mplBack.Play;
    mplBack.Notify := True;
    mplBackPlay := True;
    MediaPlayer1.Notify := True;
    imgPause.Visible := False;
    imgPauseD.Visible := False;
    imgPauseO.Visible := False;
  except;
  end;
end;

procedure TfrmMainJonbesh.lbl8MouseEnter(Sender: TObject);
begin
  img8.Visible := True;
end;

procedure TfrmMainJonbesh.lblPreviousMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := True;
end;

procedure TfrmMainJonbesh.lblPreviousMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgPreD.Visible := False;
end;

procedure TfrmMainJonbesh.lblRewindMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := True;
end;

procedure TfrmMainJonbesh.lblRewindMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRwD.Visible := False;
end;

procedure TfrmMainJonbesh.lblStopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := True;
end;

procedure TfrmMainJonbesh.lblStopMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgStopD.Visible := False;
end;

procedure TfrmMainJonbesh.lblForwardMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := True;
end;

procedure TfrmMainJonbesh.lblForwardMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgFwD.Visible := False;
end;

procedure TfrmMainJonbesh.lblNextMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := True;
end;

procedure TfrmMainJonbesh.lblNextMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgNextD.Visible := False;
end;

procedure TfrmMainJonbesh.lblRepeatMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := True;
end;

procedure TfrmMainJonbesh.lblRepeatMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgRepD.Visible := False;
end;

procedure TfrmMainJonbesh.lblLoopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := True;
end;

procedure TfrmMainJonbesh.lblLoopMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgLoopD.Visible := False;
end;

procedure TfrmMainJonbesh.lblPlayPauseMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (NowPlay <> 0) then
    if imgPause.Visible then
      imgPauseD.Visible := True
    else if not imgPause.Visible then
      imgPlayD.Visible := True;
end;

procedure TfrmMainJonbesh.lblPlayPauseMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
//  if imgPauseO.Visible then
    imgPauseD.Visible := False;
//  if imgPlay.Visible then
    imgPlayD.Visible := False;
end;

procedure TfrmMainJonbesh.lblSysTrayClick(Sender: TObject);
begin
  InitialTrayIcon;
  try
    if MediaPlayer1.Mode = mpPlaying then
      MediaPlayer1.Stop;
    if mplBack.Mode = mpPlaying then
    begin
      mplBack.Stop;
      mplBackPlay := False;
    end;
  except;end;
  Hide;
  frmBackground.Hide;
end;

procedure TfrmMainJonbesh.FormDestroy(Sender: TObject);
begin
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMainJonbesh.N1Click(Sender: TObject);
begin
  frmBackground.Show;
  frmMainJonbesh.Show;
  if mplBackPlay then
    mplBack.Play;
  Shell_NotifyIcon(NIM_DELETE, @TrayIconData);
end;

procedure TfrmMainJonbesh.N2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainJonbesh.N8Click(Sender: TObject);
begin
  if (mplBack.Mode = mpPlaying) then
  begin
    mplBack.Stop;
    mplBackPlay := False;
  end;
end;

procedure TfrmMainJonbesh.lblSysTrayMouseEnter(Sender: TObject);
begin
  imgSysTray.Visible := True;
end;

procedure TfrmMainJonbesh.lblSysTrayMouseLeave(Sender: TObject);
begin
  imgSysTray.Visible := False;
end;

procedure TfrmMainJonbesh.actFullScreenExecute(Sender: TObject);
var
  t: TRect;
begin
  try
    frmFullScreen := TfrmFullScreen.Create(Self);
    MediaPlayer1.Display := frmFullScreen.Panel1;
    with t do
    begin
      Left := 0;
      Top := 0;
      Right := frmFullScreen.Panel1.Width;
      Bottom := frmFullScreen.Panel1.Height;
    end;
    MediaPlayer1.DisplayRect := t;
//    MediaPlayer1.Display.
    frmFullScreen.ShowModal;
  except;end;
end;

procedure TfrmMainJonbesh.pnlScreenDblClick(Sender: TObject);
begin
  actFullScreen.OnExecute(Sender);
end;

procedure TfrmMainJonbesh.actMuteExecute(Sender: TObject);
begin
  if not Mute then
  begin
    mVolControl1.Volume := 0;
    imgMute.Visible := True;
    SetMute;
  end
  else
  begin
    mVolControl1.Volume := Vol;
    SoundHeightShow;
    imgMute.Visible := False;
    ResetMute;
  end;
end;

procedure TfrmMainJonbesh.lblMuteClick(Sender: TObject);
begin
  actMute.OnExecute(Sender);
end;

procedure TfrmMainJonbesh.lblMuteMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := True;
end;

procedure TfrmMainJonbesh.lblMuteMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  imgMuteD.Visible := False;
end;

procedure TfrmMainJonbesh.lblMuteMouseEnter(Sender: TObject);
begin
  imgMute.Visible := True;
end;

procedure TfrmMainJonbesh.lblMuteMouseLeave(Sender: TObject);
begin
  if not Mute then
    imgMute.Visible := False;
end;

end.
